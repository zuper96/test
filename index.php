<html>
  <head>
   <title>bagustuta.com</title>
  </head>
  <body>
    <div>
      Another function of the iframe on php and javascript. <br/>Use iframe to insert, update, delete the mysql database.
    </div>
    <div>
      &nbsp;
    </div>
    <div>
    Input Form
    <!-- set iframe visibility into hidden -->
    <iframe style="position: absolute; visibility:hidden" id="ipost"></iframe>
    <!-- iframe end -->
    </div>
   <div>
      Name : <input type="text" id="name" placeholder="Type Your Name"> &nbsp; <input type="button" id="btnsave" onclick="save(document.getElementById('name').value,document.getElementById('ids').value)" value="Save"/>
      <input type="hidden" id="ids" value="">
    </div>
    <div>
      &nbsp;
    </div>
    <div>
      Data Table
    </div>
    <div id="showdata"></div>
  </body>
</html>
<script src="https://code.jquery.com/jquery-2.1.4.js"></script> <!-- you can download this https://code.jquery.com/jquery-2.1.4.js and place into your local folder then change src="jquery-2.1.4.js" -->
<script type="text/javascript">
 
 //load data
 $('#showdata').load("data.php");
 
 //save & edit function
 function save(val,id){
  if(id==""){
   document.getElementById('ipost').src="post.php?methode=save&val="+val;
  }else{
   document.getElementById('ipost').src="post.php?methode=edit&val="+val+"&id="+id;
   }
  document.getElementById('ids').value="";
  document.getElementById('name').value="";
  $('#showdata').load("");
  $('#showdata').load("data.php").fadeIn();
  }

 //delete function
 function del(val){
  document.getElementById('ipost').src="post.php?methode=delete&val="+val;
  $('#showdata').load("");
  $('#showdata').load("data.php").fadeIn();
  }
 
</script>